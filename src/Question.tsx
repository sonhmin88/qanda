import { FC } from 'react';
/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { gray3, gray2 } from './Styles';
import { QuestionData } from './QuestionsData';
import React from 'react';
import { Link } from 'react-router-dom';

interface Props {
  data: QuestionData;
  showContent?: boolean;
}

export const Question: FC<Props> = ({ data, showContent }) => (
  <div
    css={css`
      padding: 10px 0px;
    `}
  >
    <Link
      to={`question/${data.questionId}`}
      css={css`
        padding: 10px 0px;
        font-size: 19px;
      `}
    >
      {data.title}
    </Link>

    {showContent && (
      <div>
        {data.content.length > 50
          ? `${data.content.substring(0, 50)}...`
          : data.content}
      </div>
    )}

    <div
      css={css`
        font-size: 12px;
        font-style: italic;
        color: ${gray3};
      `}
    >
      {`Asked by ${data.userName} on
    ${data.created.toLocaleDateString()} ${data.created.toLocaleTimeString()}`}
    </div>
  </div>
);
